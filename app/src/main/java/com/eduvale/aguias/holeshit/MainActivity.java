package com.eduvale.aguias.holeshit;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import com.eduvale.aguias.holeshit.adapters.BuracosAdapter;
import com.eduvale.aguias.holeshit.dao.BuracosDAO;

import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;


/*
 * Classe principal do projeto, responsável por controlar a tela de listagem principal do projeto
 *
 * Nesta classe implementamos também uma interface chamada 'OnItemClickListener', responsável por
 * manipular as ações de click em cada um dos itens da lista
 */
public class MainActivity extends AppCompatActivity implements View.OnClickListener,AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener{

    /*
     * Objeto privado do tipo ListView, responsável por controlar a lista contendo os dados dos
     * Buracos armazenados no banco de dados
     */
    private ListView listaDados;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        /*
         * Configuração do botão, de adicionar Buracos, que fica no canto inferior direito da
         * tela
         */
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(this);
    }
    /*
     * Evento OnStart do ciclo de vida de uma Activity, é executado após o OnCreate, é responsável
     * por listar os dados do banco de dados e popular a lista com as informações
     */
    @Override
    protected void onStart() {
        super.onStart();

        /*
         * Faz a referência entre o componente visual e o Objeto responsável pelo controle
         */
        listaDados = findViewById(R.id.lista_Buracos);

        /*
         * Instância a classe DAO referente aos Buracos, para que seja possível montar a lista
         * com os dados armazenados no banco de dados
         */
        BuracosDAO dao = new BuracosDAO(this);

        /*
         * Atribuíção do Adapter criado para manipular os itens da lista de Buracos, para a lista
         * a Atribuíção de um BaseAdapter, classe responsável por criar listas com conteúdo
         * personalizado, é feita através do método 'setAdapter'
         */
        listaDados.setAdapter(new BuracosAdapter(dao.carregaBuracos(), this));

        /*
         * Por fim é configurado o 'Listener', o 'Ouvinte' responsável por processar, tratar,
         * manipular a ação de click em cada um dos itens da lista
         */
        listaDados.setOnItemClickListener(this);
        listaDados.setOnItemLongClickListener(this);

    }

    /*
     * Por enquanto ainda não vamos trabalhar aqui
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    /*
     * Nem aqui
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent i= new Intent(MainActivity.this,activity_sobre.class);
            startActivity(i);

            return true;
        } else if (id == R.id.action_mapa) {

            Intent i= new Intent(MainActivity.this,MapsActivity.class);
            i.putExtra("DADOS", "LISTA" );
            startActivity(i);
        } else if (id == R.id.action_sair) {
            finish();
        } else if (id == R.id.action_idioma) {

            Intent i= new Intent(Intent.ACTION_MAIN);
            i.setClassName("com.android.settings","com.android.settings.LanguageSettings");
            startActivity(i);
        }

        return super.onOptionsItemSelected(item);
    }


    /*
     * Método público responsável por, manipular as ações de click, no botão 'inserir', que fica
     * no canto inferior direito da tela
     */
    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.fab) {
            Intent i = new Intent(MainActivity.this, CadastrarBuracos.class);
            startActivity(i);
        }
    }

    /*
     * Método público, sobrescrito, itilizado para manipular as açõess de click nos itens da lista
     */
    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int index, long id) {
        Intent i = new Intent(MainActivity.this, BuracosDetalhesActivity.class);

        /*
         * O Método 'putExtra', é utilizado para a passagem de valores entre as Activities, os dados
         * são organizado no padrão chave-valor, neste caso a chave 'Buraco', faz referência ao
         * identificador único 'chave primária', do procurado selecionado
         */

        i.putExtra("BURACO", id);
        startActivity(i);
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

        return false;
    }
}

