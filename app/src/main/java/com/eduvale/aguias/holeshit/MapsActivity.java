package com.eduvale.aguias.holeshit;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.eduvale.aguias.holeshit.dao.BuracosDAO;
import com.eduvale.aguias.holeshit.domain.Buracos;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;

import java.util.List;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }
    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        /*
         * Após o mapa estar pronto, e as permissões verificadas, o mapa é preparado, e o marcador é
         * colocado nas coordenadas informadas no momento de criação da Intent
         */
        String Acao = getIntent().getExtras().getString("DADOS");

        if (Acao.equals("LISTA")) {

            mMap.setMyLocationEnabled(true);
            mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

            BuracosDAO lista = new BuracosDAO(this);
            List<Buracos> list = lista.carregaBuracos();

            for (int i = 0; i < list.size(); i++) {

                double latitude = list.get(i).getDetalhes().getLatitude();
                double longitude = list.get(i).getDetalhes().getLongitude();
                String nomeBuraco = list.get(i).getNome();
                String snip = list.get(i).getDetalhes().getObservacoes();
                LatLng posicaoDaCaputra = new LatLng(latitude, longitude);

                if (list.get(i).getDetalhes().getrecapeado()) {
                    mMap.addMarker(new MarkerOptions()
                            .position(posicaoDaCaputra)
                            .title(nomeBuraco)
                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN))
                            .snippet("Obs: " + snip));
                } else {
                    mMap.addMarker(new MarkerOptions()
                            .position(posicaoDaCaputra)
                            .title(nomeBuraco)
                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED))
                            .snippet("Obs: " + snip));
                }

                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(posicaoDaCaputra,5));
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(posicaoDaCaputra,15),3000,null);
            }
        } else if (Acao.equals("DETALHE")) {
            mMap.setMyLocationEnabled(true);
            mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            double latitude = getIntent().getExtras().getDouble("LATITUDE");
            double longitude = getIntent().getExtras().getDouble("LONGITUDE");
            String nomeBuraco = getIntent().getExtras().getString("NOME");
            LatLng posicaoDaCaputra = new LatLng(latitude, longitude);
            mMap.addMarker(new MarkerOptions().position(posicaoDaCaputra).title(nomeBuraco));
            mMap.moveCamera(CameraUpdateFactory.newLatLng(posicaoDaCaputra));
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(posicaoDaCaputra,5));
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(posicaoDaCaputra,20),3000,null);

        } else if (Acao.equals("ESCOLHER")) {


            mMap.setMyLocationEnabled(true);
            mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            double latitude = -23.102180;
            double longitude = -48.916616;
            LatLng posicaoDaCaputra = new LatLng(latitude, longitude);
            Marker m = mMap.addMarker(new MarkerOptions()
                .position(posicaoDaCaputra)
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE))
                .draggable(true)
                .title("Definir Manualmente"));
            String idMarker = m.getId();

            mMap.moveCamera(CameraUpdateFactory.newLatLng(posicaoDaCaputra));
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(posicaoDaCaputra,20));
            //putExtraData(mMap.);


        }
    } //fim do map ready

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.popup, menu);
        return true;//super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.maptypeHYBRID:
                if(mMap != null){
                    mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
                    return true;
                }
            case R.id.maptypeNONE:
                if(mMap != null){
                    mMap.setMapType(GoogleMap.MAP_TYPE_NONE);
                    return true;
                }
            case R.id.maptypeNORMAL:
                if(mMap != null){
                    mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                    return true;
                }
            case R.id.maptypeSATELLITE:
                if(mMap != null){
                    mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                    return true;
                }
            case R.id.maptypeTERRAIN:
                if(mMap != null){
                    mMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
                    return true;
                }
            case R.id.menu_legalnotices:
                String LicenseInfo = this.getString(R.string.msg_legal_notice);
                AlertDialog.Builder LicenseDialog =
                        new AlertDialog.Builder(MapsActivity.this);
                LicenseDialog.setTitle("Legenda");
                LicenseDialog.setMessage(LicenseInfo);
                LicenseDialog.show();
                return true;
            case R.id.menu_about:
                AlertDialog.Builder aboutDialogBuilder =
                        new AlertDialog.Builder(MapsActivity.this);
                aboutDialogBuilder.setTitle("Eduvale")
                        .setMessage("http://www.eduvaleavare.com.br");

                aboutDialogBuilder.setPositiveButton(this.getString(R.string.msg_visite),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                String url = "http://www.eduvaleavare.com.br";
                                Intent i = new Intent(Intent.ACTION_VIEW);
                                i.setData(Uri.parse(url));
                                startActivity(i);
                            }
                        });

                aboutDialogBuilder.setNegativeButton(this.getString(R.string.msg_cancel),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });

                AlertDialog aboutDialog = aboutDialogBuilder.create();
                aboutDialog.show();

                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
