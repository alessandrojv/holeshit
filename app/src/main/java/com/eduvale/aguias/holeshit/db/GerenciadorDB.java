package com.eduvale.aguias.holeshit.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Alessandro on 17/04/2018.
 */

public class GerenciadorDB extends SQLiteOpenHelper {

    public GerenciadorDB(Context context) {
        super(context, "bd_holeshit", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        /*
         * comando para a criação da tabela de detalhes dos Buracos
         */
        String sql = "CREATE TABLE Buracos_DETALHES (" +
                "ID INTEGER PRIMARY KEY AUTOINCREMENT," +
                "OBSERVACOES TEXT," +
                "FOTO TEXT," +
                "LATITUDE REAL," +
                "LONGITUDE REAL," +
                "recapeado INTEGER," +
                "ID_BURACO INTEGER);";

        /*
         * Executa o comando de criação de detalhes do procurado
         */
        sqLiteDatabase.execSQL(sql);

        /*
         * Comando para a criação da tabela de Buracos
         */
        sql = "CREATE TABLE Buracos (" +
                "ID INTEGER PRIMARY KEY AUTOINCREMENT," +
                "NOME TEXT," +
                "Cidade TEXT," +
                "Bairro TEXT," +
                "CEP TEXT," +
                "numero TEXT);";

        /*
         * Executa o comando de criação da tabela de Buracos
         */
        sqLiteDatabase.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}