package com.eduvale.aguias.holeshit;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class activity_sobre extends AppCompatActivity implements View.OnClickListener {

    private TextView lbl_luz;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sobre);

        lbl_luz = findViewById(R.id.txtProf);

        lbl_luz.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        if (v.getId()==R.id.txtProf) {
            Toast.makeText(this,"Olha esse raio de luz!",Toast.LENGTH_LONG).show();        }
    }
}
