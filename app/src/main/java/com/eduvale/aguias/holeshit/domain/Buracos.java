package com.eduvale.aguias.holeshit.domain;

/**
 * Created by Alessandro on 17/04/2018.
 */

public class Buracos {

    private Long id;
    private String nome;
    private String bairro;
    private String cidade;
    private String cep;
    private String numero;
    private BuracosDetalhes detalhes;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getCep() { return cep; }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getnumero() {
        return numero;
    }

    public void setnumero(String numero) {
        this.numero = numero;
    }

    public BuracosDetalhes getDetalhes() {
        return detalhes;
    }

    public void setDetalhes(BuracosDetalhes detalhes) {
        this.detalhes = detalhes;
    }
}
