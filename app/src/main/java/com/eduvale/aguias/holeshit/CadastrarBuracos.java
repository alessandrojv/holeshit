package com.eduvale.aguias.holeshit;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.eduvale.aguias.holeshit.dao.BuracosDAO;
import com.eduvale.aguias.holeshit.dao.DetalhesDAO;
import com.eduvale.aguias.holeshit.domain.Buracos;
import com.eduvale.aguias.holeshit.domain.BuracosDetalhes;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;

import java.io.IOException;
import java.util.List;

/*
 * Activity responsável por controlar o cadastro de novos Buracos no banco de dados interno da
 * aplicação
 */
public class CadastrarBuracos extends AppCompatActivity implements View.OnClickListener {

    private static final int PLACE_PICKER_REQUEST = 1;
    /*
     * Objetos usados para fazer referência aos componentes visuais
     */
    private EditText txtEndereco;
    private EditText txtCidade;
    private EditText txtBairro;
    private EditText txtCEP;
    private EditText txtnumero;
    private TextView txtLatitude;
    private TextView txtLongitude;
    private TextView txtAltitude;
    private Button btnSalvar;
    private Button btnBuscar;
    private Button btnEscolher;
    private Location location;
    private LocationManager locationManager;

    private final int ACAO_ESCOLHER_MAPA = 1;


    /*
     * Objeto DAO, responsável por controlar os dados dos objetos que deverão ser persistidos no
     * banco de dados interno do dispositivo
     */
    private BuracosDAO dao;
    private double latitude = 0.00;
    private double longitude = 0.00;
    private double altitude = 0.00;

    public Address buscarEndereco(double latitude, double longitude) throws IOException {

        Geocoder geocoder;
        Address address = null;
        List<Address> addresses;

        geocoder = new Geocoder(getApplicationContext());

        addresses = geocoder.getFromLocation(latitude,longitude,1);
        if (addresses.size() > 0) {
            address = addresses.get(0);
        }
        return address;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastrar_buracos);

        /*
         * Referência entre os componentes visuais e os objetos de referência no código
         */
        txtEndereco = (EditText) findViewById(R.id.txt_endereco);
        txtCidade = (EditText) findViewById(R.id.txt_cidade);
        txtBairro = (EditText) findViewById(R.id.txt_bairro);
        txtCEP = (EditText) findViewById(R.id.txt_cep);
        txtnumero = (EditText) findViewById(R.id.txt_numero);
        txtLatitude = (TextView) findViewById(R.id.txtLatitude);
        txtLongitude = (TextView) findViewById(R.id.txtLongitude);
        txtAltitude = (TextView) findViewById(R.id.txtAltitude);

        btnSalvar = (Button) findViewById(R.id.btn_salvar);
        btnBuscar = (Button) findViewById(R.id.btn_buscar);
        btnEscolher = (Button) findViewById(R.id.btn_escolher);
        btnSalvar.setOnClickListener(this);
        btnBuscar.setOnClickListener(this);
        btnEscolher.setOnClickListener(this);

        /*
         * Instância o objeto BuracosDAO
         */
        dao = new BuracosDAO(this);

        checaPermissoes();

    }

    private void checaPermissoes() {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        }

    }

    /*
     * método privado utilizado para ler a ultima posição registrada pelo dispositivo, caso não haja
     * nenhuma ou as permissões não estejam de acordo, o valor null é retornado
     */
    public Location pegarUltimaPosicaoConhecida() {
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            checaPermissoes();
            return null;
        }
        assert locationManager != null;
        return locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {

            try {
                //Place place = PlacePicker.getPlace(data, this);
                Place place = PlacePicker.getPlace(this, data);

                txtEndereco.setText(place.getAddress());
                //txtBairro.setText("...");
                //txtCidade.setText("...");
                //txtCEP.setText("18700");
                //txtnumero.setText("0");
                latitude = place.getLatLng().latitude;
                longitude = place.getLatLng().longitude;
                txtLatitude.setText(Double.toString(place.getLatLng().latitude));
                txtLongitude.setText(Double.toString(place.getLatLng().longitude));
                txtAltitude.setText("0.00");

                try {

                    Address endereco = buscarEndereco(latitude, longitude);
                    //txtEndereco.setText(endereco.getAddressLine(0));
                    txtBairro.setText(endereco.getSubLocality());
                    txtCidade.setText(endereco.getLocality());
                    txtCEP.setText(endereco.getPostalCode());
                    txtnumero.setText(endereco.getSubThoroughfare());
                } catch (IOException e) {
                    Toast.makeText(this, "Endereço não localizado. Motivo: "+e.getMessage(), Toast.LENGTH_SHORT).show();
                }

                Toast.makeText(this, getResources().getString(R.string.msg_obtido), Toast.LENGTH_SHORT).show();
                //String toastMsg = String.format("Place: %s", place.getName());
                //Toast.makeText(this, toastMsg, Toast.LENGTH_LONG).show();
            } catch (Exception e) {
                Toast.makeText(this, getResources().getString(R.string.msg_n_obtido), Toast.LENGTH_LONG).show();
            }


            }
        }
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.btn_salvar){

            try {
            Buracos buraco = new Buracos();

            buraco.setNome(txtEndereco.getText().toString());
            buraco.setBairro(txtBairro.getText().toString());
            buraco.setCidade(txtCidade.getText().toString());
            buraco.setCep(txtCEP.getText().toString());
            buraco.setnumero(txtnumero.getText().toString());

            /*
             * Insere os valores padrão para os detalhes do buraco
             */
            BuracosDetalhes detalhes = new BuracosDetalhes();
            detalhes.setrecapeado(false);
            detalhes.setFoto("");
            detalhes.setLatitude(latitude);
            detalhes.setLongitude(longitude);
            detalhes.setObservacoes("");

                /*
                 * Insere o Buraco no banco de dados, e à partir da chave primária correspondente
                 * ao item recém inserido, seus detalhes também são armazenados no banco de dados
                 */
                Long idBuraco =  dao.insertBuracos(buraco);
                detalhes.setId_buraco(idBuraco);

                DetalhesDAO detalhesDAO = new DetalhesDAO(CadastrarBuracos.this);
                detalhesDAO.InserirDetalhes(detalhes);

                /*
                 * Caso tudo tenha ocorrido sem erros uma mensagem de sucesso é exibida
                 */
                Toast.makeText(CadastrarBuracos.this,"Buraco Cadastrado com Sucesso!",Toast.LENGTH_LONG).show();
                finish();

            }catch(Exception e){
                /*
                 * Caso algo tenha saído errado, uma mensagem de erro é exibida
                 */
                Toast.makeText(CadastrarBuracos.this,"Falha ao Cadastrar o Buraco!",Toast.LENGTH_LONG).show();
            }
        } else if (view.getId() == R.id.btn_buscar){

            try {

            Location l = pegarUltimaPosicaoConhecida();
            if(l != null) {
                latitude =  l.getLatitude();
                longitude = l.getLongitude();
                altitude = l.getAltitude();
            }else{
                latitude = 0.0;
                longitude = 0.0;
                altitude = 0.0;
                Toast.makeText(this, "Falha ao obter a localização", Toast.LENGTH_SHORT).show();
            }

            txtLatitude.setText(Double.toString(latitude));
            txtLongitude.setText(Double.toString(longitude));
            txtAltitude.setText(Double.toString(altitude));



                Address endereco = buscarEndereco(latitude, longitude);
                txtEndereco.setText(endereco.getAddressLine(0));
                txtBairro.setText(endereco.getSubLocality());
                txtCidade.setText(endereco.getLocality());
                txtCEP.setText(endereco.getPostalCode());
                txtnumero.setText(endereco.getSubThoroughfare());


            } catch (IOException e) {
                Toast.makeText(this, "Falha ao obter a localização. Motivo: "+e.getMessage(), Toast.LENGTH_SHORT).show();
            }

        } else if (view.getId() == R.id.btn_escolher) {

            int PLACE_PICKER_REQUEST = 1;
            PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();

            try {
                startActivityForResult(builder.build(this), PLACE_PICKER_REQUEST);
            } catch (GooglePlayServicesRepairableException e) {
                e.printStackTrace();
            } catch (GooglePlayServicesNotAvailableException e) {
                e.printStackTrace();
            }


            //Intent i= new Intent(CadastrarBuracos.this,MapsActivity.class);
            //i.putExtra("DADOS", "ESCOLHER" );
            //startActivityForResult(i,ACAO_ESCOLHER_MAPA);
        }
    }
}
