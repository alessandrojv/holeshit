package com.eduvale.aguias.holeshit;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class SplashFullscreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_fullscreen);

        Handler handle = new Handler();
        handle.postDelayed(new Runnable() {
            @Override
            public void run() {
                mostrarMain();
            }
        },4000); // tempo de exibição

    }

    private void mostrarMain() {
        Intent intent = new Intent(SplashFullscreen.this,MainActivity.class);
        startActivity(intent);
        finish();

    }
}
