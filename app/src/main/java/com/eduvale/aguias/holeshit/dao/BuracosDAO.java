package com.eduvale.aguias.holeshit.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.eduvale.aguias.holeshit.db.GerenciadorDB;
import com.eduvale.aguias.holeshit.domain.Buracos;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alessandro on 24/04/2018.
 */

public class BuracosDAO {

    /*
     * Declara o objeto responsável por gerenciar as conexões
     * com o banco de dados
     */
    private GerenciadorDB db;
    private Context context;

    /*
     * Método Construtor, que inicializa todos os objetos necessários
     * para o correto funcionamento da classe;
     */
    public BuracosDAO(Context context) {
        this.context = context;
        this.db = new GerenciadorDB(this.context);
    }

    public Long insertBuracos(Buracos buraco) throws Exception {
        /*
         * Objeto responsável por, organizar os valores
         * no padrão 'chave/valor'
         */
        ContentValues contentValues = new ContentValues();

        contentValues.put("NOME", buraco.getNome());
        contentValues.put("numero", buraco.getnumero());
        contentValues.put("Cidade", buraco.getCidade());
        contentValues.put("Bairro", buraco.getBairro());
        contentValues.put("CEP", buraco.getCep());

        return this.db.getWritableDatabase().insert("Buracos",
                null,
                contentValues);
    }

    public List<Buracos> carregaBuracos() {
        /*numero
         * Objeto String utilizado para armazenar a consulta
         * SQL que será executada
         */
        String sql = "SELECT * FROM Buracos";

        /*
         * Executa a consulta e grava o resultado no objeto cursor
         */
        Cursor cursor = this.db.getReadableDatabase().rawQuery(sql,
                null);

        /*
         * move o cursor, para a primeira posição
         */
        cursor.moveToFirst();

        /*
         * Cria a lista que vai ser responsável por armazenar os
         * dados da tabela
         */
        List<Buracos> retorno = new ArrayList<>();

        /*
         * Realiza o loop responsável por carregar todos os dados da tabela;
         */
        if (cursor.getCount() > 0)
            do {
                Buracos item = new Buracos();
                item.setId(cursor.getLong(cursor.getColumnIndex("ID")));
                item.setNome(cursor.getString(cursor.getColumnIndex("NOME")));
                item.setCidade(cursor.getString(cursor.getColumnIndex("Cidade")));
                item.setBairro(cursor.getString(cursor.getColumnIndex("Bairro")));
                item.setCep(cursor.getString(cursor.getColumnIndex("CEP")));
                item.setnumero(cursor.getString(cursor.getColumnIndex("numero")));

                /*
                 * Carrega o objeto de detalhes do buraco
                 */
                DetalhesDAO daoDetalhes = new DetalhesDAO(this.context);
                item.setDetalhes(daoDetalhes.buscaPorBuraco(item));

                retorno.add(item);
            } while (cursor.moveToNext());


        return retorno;
    }


    public Buracos carregaBuracosPorID(Buracos Buracos) {
/*
         * Objeto String utilizado para armazenar a consulta
         * SQL que será executada
         */
        String sql = "SELECT * FROM Buracos WHERE ID = @idBuraco";

        /*
         * Executa a consulta e grava o resultado no objeto cursor
         */
        Cursor cursor = this.db.getReadableDatabase().rawQuery(sql, new String[]{String.valueOf(Buracos.getId())});

        /*
         * move o cursor, para a primeira posição
         */
        cursor.moveToFirst();

        /*
         * Cria a lista que vai ser responsável por armazenar os
         * dados da tabela
         */
        List<Buracos> retorno = new ArrayList<>();

        /*
         * Realiza o loop responsável por carregar todos os dados da tabela;
         */
        if (cursor.getCount() > 0) {

            Buracos item = new Buracos();
            item.setId(cursor.getLong(cursor.getColumnIndex("ID")));
            item.setNome(cursor.getString(cursor.getColumnIndex("NOME")));
            item.setCidade(cursor.getString(cursor.getColumnIndex("Cidade")));
            item.setBairro(cursor.getString(cursor.getColumnIndex("Bairro")));
            item.setCep(cursor.getString(cursor.getColumnIndex("CEP")));
            item.setnumero(cursor.getString(cursor.getColumnIndex("numero")));

                /*
                 * Carrega o objeto de detalhes do buraco
                 */
            DetalhesDAO daoDetalhes = new DetalhesDAO(this.context);
            item.setDetalhes(daoDetalhes.buscaPorBuraco(item));


            return item;
        }else{
            return null;
        }
    }

}
