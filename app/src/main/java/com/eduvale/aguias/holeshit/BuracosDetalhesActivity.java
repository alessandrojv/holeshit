package com.eduvale.aguias.holeshit;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.eduvale.aguias.holeshit.dao.BuracosDAO;
import com.eduvale.aguias.holeshit.dao.DetalhesDAO;
import com.eduvale.aguias.holeshit.domain.Buracos;
import com.eduvale.aguias.holeshit.domain.BuracosDetalhes;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;

/**
 * Created by Alessandro on 29/04/2018.
 */

/*
 * Classe pública responsável por controlar os detalhes dos dados referentes ao buraco
 * selecionado na lista
 */
public class BuracosDetalhesActivity extends AppCompatActivity implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {

    private TextView nomeBuraco;
    private TextView numero;
    private EditText observacoes;
    private Switch recapeado;
    private ImageButton image;
    private Button btnMapa;
    private Button btnApagar;
    private Button btnGaleria;

    private BuracosDetalhes detalhes;

    private final int ACAO_TIRAR_FOTO = 1;
    private final int ACAO_GALERIA = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.buracos_detalhes);

        /*
         * Referência dos componentes visuais, com os objetos de controle
         */
        this.nomeBuraco = findViewById(R.id.lbl_endereco);
        this.numero = findViewById(R.id.lbl_numero);
        this.observacoes = findViewById(R.id.edtObservacoes);
        this.recapeado = findViewById(R.id.sw_recapeado);
        this.recapeado.setOnCheckedChangeListener(this);
        this.image = findViewById(R.id.img_foto);
        this.image.setOnClickListener(this);
        this.btnMapa = (Button) findViewById(R.id.btn_mapa);
        this.btnApagar = (Button) findViewById(R.id.btn_apagar);
        this.btnGaleria = (Button) findViewById(R.id.btn_galeria);
        this.btnMapa.setOnClickListener(this);
        this.btnApagar.setOnClickListener(this);
        this.btnGaleria.setOnClickListener(this);

        checaPermissoes();

    }

    /*
     * Método privado utilizado para solicitar as permissões de localização ao usuário, necessário à
     * partir da versão 6 do android, além das permissões do arquivo Manifest estas são necessárias
     * apenas apra efeitos de segurança
     */
    private void checaPermissoes() {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        }

    }

    /*
     * método privado utilizado para ler a ultima posição registrada pelo dispositivo, caso não haja
     * nenhuma ou as permissões não estejam de acordo, o valor null é retornado
     */
    public Location pegarUltimaPosicaoConhecida() {
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            checaPermissoes();
            return null;
        }
       return locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
    }

    /*
     * Evento OnStart do ciclo de vida de uma Activity, é executado após o OnCreate, é responsável
     * por recuperar os dados do buraco selecionado a partir do id (chave primária) no banco de
     * dados
     */
    @Override
    protected void onStart() {
        super.onStart();


        /*
         * Primeiro instanciamos um objeto, Procurardos, e então através dos métodos 'getIntent()' e
         * então o método 'getExtras()', podemos recuperar os valores a partir de suas chaves,
         * neste caso recuperaremos os o id do Buraco selecionado, a partir da chave que foi
         * informada na MainActivity
         */
        Buracos itemSelecionado = new Buracos();
        itemSelecionado.setId(getIntent().getExtras().getLong("BURACO"));

        /*
         * Em seguida a partir de um Objeto, DAO, recueraremos todas as informações do Buracos no
         * banco de dados a partir do id (chave primária)
         */
        BuracosDAO dao = new BuracosDAO(this);
        itemSelecionado =  dao.carregaBuracosPorID(itemSelecionado);

        this.nomeBuraco.setText(itemSelecionado.getNome());
        this.numero.setText(String.valueOf(itemSelecionado.getnumero()));
        this.observacoes.setText(itemSelecionado.getDetalhes().getObservacoes());
        this.recapeado.setChecked(itemSelecionado.getDetalhes().getrecapeado());

        /*
         * O problema da não exibição era devido ao ciclo de vida, sempre que a câmera era finalizada
         * os daods carregados eram sempre os dados que ainda não tinham a foto capturada, a estrutura
         * abaixo permite que os daods sejam utilizados da maneira correta
         */
        if(this.detalhes == null) {
            this.detalhes = itemSelecionado.getDetalhes();
            exibirFoto();
        }else{
            exibirFoto();
        }

    }

    private void exibirFoto(){
        byte foto[] = Base64.decode(this.detalhes.getFoto().getBytes(),
                Base64.DEFAULT);
        Bitmap s = BitmapFactory.decodeByteArray(foto, 0, foto.length);

        if(s != null)
            this.image.setImageBitmap(s);
    }

    /*
     * O método 'onBackPressed()', é executado, quando o botão 'voltar' dos dispositivos android é
     * pressionado, neste caso iremos além de voltar a tela anterior atualizar as informações que
     * podem ter sido alteradas nesta tela
     */
    @Override
    public void onBackPressed() {
        super.onBackPressed();

        /*
         * Primeiramente as informações são recuperadas, a partir dos componentes visuais
         */
        this.detalhes.setrecapeado(this.recapeado.isChecked());
        this.detalhes.setObservacoes(this.observacoes.getText().toString());

        /*
         * Em seguida um objeto DAO para a manipulação dos dados dos detalhes, é instanciado e os
         * dados alterados são atualizados na base de dados
         */
        DetalhesDAO daoDetalhes = new DetalhesDAO(this);
        daoDetalhes.atualizarBuraco(this.detalhes);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == ACAO_TIRAR_FOTO){

            try {
            Bundle extras = data.getExtras();
            Bitmap foto = (Bitmap) extras.get("data");
            image.setImageBitmap(foto);

            ByteArrayOutputStream byteArrayOutputStream =
                    new ByteArrayOutputStream();
            foto.compress(Bitmap.CompressFormat.PNG,
                    100, byteArrayOutputStream);
            byte[] byteArray = byteArrayOutputStream.toByteArray();

            String encoded = Base64.encodeToString(byteArray,
                    Base64.DEFAULT);
            detalhes.setFoto(encoded); }
            catch (Exception e) {
                Toast.makeText(this,"Não foi possível realizar a captura. Tente Novamente",Toast.LENGTH_LONG);
            }
        } else if (requestCode == ACAO_GALERIA) {

            try {
                final Uri imageUri = data.getData();
                final InputStream imageStream = getContentResolver().openInputStream(imageUri);
                final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                image.setImageBitmap(selectedImage);

                ByteArrayOutputStream byteArrayOutputStream =
                        new ByteArrayOutputStream();
                selectedImage.compress(Bitmap.CompressFormat.PNG,
                        100, byteArrayOutputStream);
                byte[] byteArray = byteArrayOutputStream.toByteArray();

                String encoded = Base64.encodeToString(byteArray,
                        Base64.DEFAULT);
                detalhes.setFoto(encoded);
            } catch (Exception e) {
                Toast.makeText(this, "Não foi recuperar a imagem. Tente Novamente", Toast.LENGTH_LONG);
            }
        }
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.img_foto){
            Intent tirarFoto = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            if(tirarFoto.resolveActivity(getPackageManager()) != null){
                startActivityForResult(tirarFoto,ACAO_TIRAR_FOTO);
            }} else if (view.getId() == R.id.btn_galeria){
            Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
            photoPickerIntent.setType("image/*");
            startActivityForResult(photoPickerIntent,ACAO_GALERIA);

        }else if (view.getId() == R.id.btn_mapa){
           /*
            * São passados para a Activity do Mapa, os valores de Latitude, Longitude e o nome do
            * buraco, utilizados para montar o marcador de maneira correta no mapa
            */
            Intent i= new Intent(BuracosDetalhesActivity.this,MapsActivity.class);
            i.putExtra("DADOS", "DETALHE");
            i.putExtra("LATITUDE",this.detalhes.getLatitude());
            i.putExtra("LONGITUDE",this.detalhes.getLongitude());
            i.putExtra("NOME",this.nomeBuraco.getText());
            startActivity(i);
        } else if (view.getId()== R.id.btn_apagar) {

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setCancelable(true);
            builder.setTitle("Eliminar");
            builder.setMessage("Deseja eliminar o registro?");
            builder.setPositiveButton("Confirmar",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            DetalhesDAO daoDetalhes = new DetalhesDAO(BuracosDetalhesActivity.this);
                            daoDetalhes.removeBuraco(detalhes);
                            Toast.makeText(BuracosDetalhesActivity.this,"Registro Eliminado",Toast.LENGTH_LONG);
                            finish();
                        }
                    });
            builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                       Toast.makeText(BuracosDetalhesActivity.this,"Registro Não Eliminado",Toast.LENGTH_LONG);
                }
            });

            AlertDialog dialog = builder.create();
            dialog.show();


        }
    }

    /*
     * Este método serve para capturar o evento de mudança da 'chave' (switch), que informa se o
     * buraco foi recapeado ou não
     */
    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
       /*
        * previne as tentativas de manipulação em um objeto nulo
        */
        if(this.detalhes != null) {
           if (b) {

               // neste momento não quero que grave as coordenadas do buraco pois já está sendo feito no cadastro
               /*
               Location l = pegarUltimaPosicaoConhecida();
               if(l != null) {
                   this.detalhes.setLatitude(l.getLatitude());
                   this.detalhes.setLongitude(l.getLongitude());
               }else{
                   this.detalhes.setLatitude(0.0);
                   this.detalhes.setLongitude(0.0);
                   Toast.makeText(this, "Falha ao obter a localização", Toast.LENGTH_SHORT).show();
               }
           } else {
               this.detalhes.setLatitude(0.0);
               this.detalhes.setLongitude(0.0); */
           }

           this.detalhes.setrecapeado(b);
           this.detalhes.setObservacoes(this.observacoes.getText().toString());

           DetalhesDAO daoDetalhes = new DetalhesDAO(this);
           daoDetalhes.atualizarBuraco(this.detalhes);
       }
    }
}