package com.eduvale.aguias.holeshit.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.eduvale.aguias.holeshit.db.GerenciadorDB;
import com.eduvale.aguias.holeshit.domain.Buracos;
import com.eduvale.aguias.holeshit.domain.BuracosDetalhes;

import java.text.SimpleDateFormat;
import java.util.Date;

/*
 * Classe DAO, responsável por manipular os daods referentes aos detalhes dos Buracos no banco de
 * dados interno da aplicação
 */
public class DetalhesDAO {

    private GerenciadorDB db;

    /*
     * Método construtor, utilizado para inicializar o objeto 'db', responsável por controlar as
     * operações envolvendo o banco de dados interno da aplicação
     */
    public DetalhesDAO(Context context) {
        db = new GerenciadorDB(context);
    }

    /*
     * Método responsável por inserir os dados referentes aos detalhes do buraco no banco de
     * dados, o método retorna
     */
    public Long InserirDetalhes(BuracosDetalhes detalhes) {
          /*
           * Objeto responsável por, organizar os valores
           * no padrão 'chave/valor'
           */
        ContentValues contentValues = new ContentValues();

        /*
         * Em um primeiro momento os detalhes são inseridos com valores padrão
         */
        detalhes.setrecapeado(false);
        detalhes.setFoto("");
        //detalhes.setLatitude(0D);
        //detalhes.setLongitude(0D);
        Date dataHoraAtual = new Date();
        String currentTime = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(dataHoraAtual);
        detalhes.setObservacoes(currentTime.toString());

        contentValues.put("OBSERVACOES", detalhes.getObservacoes());
        contentValues.put("FOTO", detalhes.getFoto());
        contentValues.put("LATITUDE", detalhes.getLatitude());
        contentValues.put("LONGITUDE", detalhes.getLongitude());

        if (detalhes.getrecapeado()) {
            contentValues.put("recapeado", 1);
        } else {
            contentValues.put("recapeado", 0);
        }
        contentValues.put("ID_BURACO", detalhes.getId_buraco());


        /*
         * o método insert, retorna a chave primária atribuiída ao registro recém inserido, desta
         * maneira o valor retornado é a chave primária do registro
         */
        return this.db.getWritableDatabase().insert("Buracos_DETALHES",
                null,
                contentValues);
    }

    /*
     * Método público, responsável por buscar detalhes a partir dos dados do buraco, caso por
     * algum motivo não seja encontrado nenhum detalhe para o buraco selecionado, um valor
     * nulo é retornado
     */
    public BuracosDetalhes buscaPorBuraco(Buracos buraco) {
        /*
         * String sql contendo o comando SQL, e um parâmetro nomeado como "@id_buraco"
         */
        String sql = "SELECT * FROM Buracos_DETALHES WHERE ID_BURACO = @id_buraco";

        /*
         * no método 'rawQuery', é informado um parâmetro do tipo String[] contendo os valores dos
         * parâmetros na ordem em que aparecem na String que contém o SQL;
         */
        Cursor cursor = this.db.getReadableDatabase().rawQuery(sql, new String[]{String.valueOf(buraco.getId())});

        cursor.moveToFirst();

        if(cursor.getCount() > 0){
            BuracosDetalhes resultado = new BuracosDetalhes();

            resultado.setId(cursor.getLong(cursor.getColumnIndex("ID")));
            resultado.setObservacoes(cursor.getString(cursor.getColumnIndex("OBSERVACOES")));
            resultado.setFoto(cursor.getString(cursor.getColumnIndex("FOTO")));
            resultado.setLatitude(cursor.getDouble(cursor.getColumnIndex("LATITUDE")));
            resultado.setLongitude(cursor.getDouble(cursor.getColumnIndex("LONGITUDE")));
            if(cursor.getInt(cursor.getColumnIndex("recapeado")) == 1)
                resultado.setrecapeado(true);
            else
                resultado.setrecapeado(false);
            resultado.setId_buraco(cursor.getLong(cursor.getColumnIndex("ID_BURACO")));

            cursor.close();
            db.close();
            return resultado;
        }
        return null;
    }

    /*
     * Método público responsável por atualizar os dados dos detalhes do buraco, o Parâmetro
     * contém todos os dados que devem ser atualizados no banco de dados
     */
    public void atualizarBuraco(BuracosDetalhes detalhes){
            /*
           * Objeto responsável por, organizar os valores
           * no padrão 'chave/valor'
           */
        ContentValues contentValues = new ContentValues();

        detalhes.setrecapeado(detalhes.getrecapeado());
        detalhes.setFoto(detalhes.getFoto());
        detalhes.setLatitude(detalhes.getLatitude());
        detalhes.setLongitude(detalhes.getLongitude());
        detalhes.setObservacoes(detalhes.getObservacoes());

        contentValues.put("OBSERVACOES", detalhes.getObservacoes());
        contentValues.put("FOTO", detalhes.getFoto());
        contentValues.put("LATITUDE", detalhes.getLatitude());
        contentValues.put("LONGITUDE", detalhes.getLongitude());

        if (detalhes.getrecapeado()) {
            contentValues.put("recapeado", 1);
        } else {
            contentValues.put("recapeado", 0);
        }
        contentValues.put("ID_BURACO", detalhes.getId_buraco());


        this.db.getWritableDatabase().update("Buracos_DETALHES",contentValues,"ID="+detalhes.getId(),null);
        this.db.close();
    }

    public void removeBuraco(BuracosDetalhes detalhes){

        this.db.getWritableDatabase().delete("Buracos_DETALHES","ID="+detalhes.getId(),null);
        this.db.getWritableDatabase().delete("Buracos","ID="+detalhes.getId_buraco(),null);
        this.db.close();
    }

}
