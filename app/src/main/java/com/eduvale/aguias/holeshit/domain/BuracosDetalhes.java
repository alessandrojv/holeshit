package com.eduvale.aguias.holeshit.domain;

/**
 * Created by Alessandro on 17/04/2018.
 */

public class BuracosDetalhes {

    private Long id;
    private Long id_buraco;
    private String observacoes;
    private String foto;
    private Double latitude;
    private Double longitude;
    private Boolean recapeado;

    public Long getId_buraco() {
        return id_buraco;
    }

    public void setId_buraco(Long id_buraco) {
        this.id_buraco = id_buraco;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getObservacoes() {
        return this.observacoes;
    }

    public void setObservacoes(String observacoes) {
        this.observacoes = observacoes;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Boolean getrecapeado() {
        if(recapeado != null)
            return recapeado;
        else
            return false;
    }

    public void setrecapeado(Boolean recapeado) {
        this.recapeado = recapeado;
    }
}
