package com.eduvale.aguias.holeshit.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.eduvale.aguias.holeshit.R;
import com.eduvale.aguias.holeshit.domain.Buracos;

import java.util.List;

/*
 * Classe responsável por adaptar um layout específico aos itens da lista de procurado
 */
public class BuracosAdapter extends BaseAdapter {

    /*
     * atributos utilizados para realizar a adaptação dos dados ao Layout escolhido para cada item
     * da lista de dados
     */
    private List<Buracos> dados;
    private Context context;

    /*
     * Método construtor, responsável por inicializar os atributos privados da classe
     */
    public BuracosAdapter(List<Buracos> dados, Context context) {
        this.dados = dados;
        this.context = context;
    }

    /*
     * Método público, sobrescrito, utilizado para informar à ListView o total de itens a serem
     * exibidos
     */
    @Override
    public int getCount() {
        /*
         * Caso a lista não esteja preenchida é retornado um item, informando que não há dados
         * para serem exibidos na lista
         */
        if (this.dados.size() == 0) {
            return 1;
        } else {
            /*
             * Caso hajam dados, o tamanho total da lista é retornado
             */
            return this.dados.size();
        }
    }

    /*
     * Método público, sobrescrito, utilizado para recuoerar um determinado item da lista, à partir
     * de um índice específico
     */
    @Override
    public Object getItem(int i) {
        /*
         * Caso não hajam dados na lista, a String "vazio", é retornada
         */
        if (this.dados.size() == 0) {
            return "VAZIO";
        } else {
            /*
             * caso hajam dados, o objeto correspondente ao índice da lista é retornado
             */
            return this.dados.get(i);
        }
    }

    /*
     * Método público, sobrescrito, utilizado para ler o ID (Identificado Único) de um determinado
     * item à partir de um índice
     */
    @Override
    public long getItemId(int i) {
        /*
         * caso não hajam dados, o identificador a ser retornado é '-1'
         */
        if (this.dados.size() == 0) {
            return -1;
        } else {
            /*
             * caso hajam dados o valor retornado é a chave primária de cada item do banco de dados
             */
            return this.dados.get(i).getId();
        }
    }


    /*
     * Método público, sobrescrito utilizado para recuperar a View correspondente a cada item da lista
     * este método é utilizado para contruir cada um dos itens da lista de dados
     */
    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        /*
         * Objeto do tipo LayoutInflater, objeto responsável por 'inflar' os arquivos XML de Layout
         * tornando possível o carregamento de um layout específico para cada item da lista
         */
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        /*
         * Objeto do tipo 'View', responsável por guardar o layout do item já inflado, é através dele
         * que os componentes visuais são linkados aos objetos de referência do código
         */
        View v = inflater.inflate(R.layout.item_buracos, viewGroup, false);

        /*
         * Objeto do tipo TextView, responsável por exibir o nome do procurado
         */
        TextView nome = (TextView) v.findViewById(R.id.endereco);
        TextView observ = (TextView) v.findViewById(R.id.txt_observ);

        /*
         * Objeto do tipo TextView, responsável por exibir o status de cada um dos Buracos,
         * 'recapeado' ou 'não recapeado'
         */
        TextView recapeado = (TextView) v.findViewById(R.id.status_recapeado);

        ImageView image = (ImageView) v.findViewById(R.id.img_buraco);

        /*
         * Caso hajam dados na lista, o dado referente ao índice 'i', é recuperado e seus dados são
         * exibidos na lista
         */
        if (this.dados.size() > 0) {
            Buracos item = dados.get(i);

            nome.setText(item.getNome());
            observ.setText(item.getDetalhes().getObservacoes());
            if (item.getDetalhes().getrecapeado()) {
                recapeado.setText(R.string.label_recapeado);
            } else {
                recapeado.setText(R.string.label_nao_recapeado);
            }

            byte foto[] = Base64.decode(item.getDetalhes().getFoto().getBytes(),
                    Base64.DEFAULT);
            Bitmap s = BitmapFactory.decodeByteArray(foto, 0, foto.length);

            if(s != null)
                image.setImageBitmap(s);

        }else{
            /*
             * Caso a lista esteja vazia são exibidos valores 'vazios', para informar ao usuário
             * que não existem dados para listagem
             */
            nome.setText(R.string.label_vazio);
            observ.setText(R.string.label_vazio);
            recapeado.setText(R.string.label_vazio);
        }

        /*
         * Retorna a view, correspondente a cada um dos itens da Lista
         */
        return v;
    }
}
